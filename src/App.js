import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Geolocation from './Geolocation'


const publicIp = require('public-ip');

(async () => {

  console.log(await publicIp.v4())

  const ip = await publicIp.v4()
  console.log({ ip })
  const response = await fetch("https://cors-anywhere.herokuapp.com/http://www.geoplugin.net/json.gp?ip=" + ip)
  const data = await response.json()
  console.log(data)



})()

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <div style={{ backgroundColor: '#fff', color: '#cccc' }}>
            <Geolocation />
          </div>
        </header>
      </div>
    );
  }
}

export default App;
